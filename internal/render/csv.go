package render

import (
	"encoding/csv"
	"fmt"
	"os"
)

type CSVRenderer struct {
	obj interface{}
}

func NewCSVRenderer(obj interface{}) *CSVRenderer {
	return &CSVRenderer{obj}
}

func (r *CSVRenderer) Render(col int, comment rune) ([][]string, error) {
	str := fmt.Sprintf("%v", r.obj)
	f, err := os.Open(str)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	reader := csv.NewReader(f)
	reader.FieldsPerRecord = col
	reader.Comment = comment
	record, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}
	return record, nil
}
