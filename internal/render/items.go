package render

import (
	"bytes"
	"encoding/json"
	"fmt"
	"text/template"
)



type JsonRenderer struct {
	obj interface{}
}

type HTMLRenderer struct {
	obj interface{}
}

func NewJsonRenderer(obj interface{}) *JsonRenderer {
	return &JsonRenderer{obj}
}

func NewHTMLRenderer(obj interface{}) *HTMLRenderer {
	return &HTMLRenderer{obj}
}

func (r *JsonRenderer) Render() ([]byte, error) {
	content, err := json.Marshal(r.obj)
	if err != nil {
		// TODO:log.Printf("Ошибка преобразования JSON: %w", err)
		return nil, fmt.Errorf("Ошибка преобразования JSON: %w", err)
	}
	return content, nil
}

func (r *HTMLRenderer) Render() ([]byte, error) {
	// Name        string  `json:"Name"`
	// Description string  `json:"Description"`
	// Сategory string `json:"Сategory"`
	// PriceOrigin       float32 `json:"Price"`
	// PriceDiscount float32 `json:"PriceDiscount"`
	// Discount float32 `json:"Discount"`
	// ProductOfDay bool `json:"ProductOfDay"`

	html := `<table>
				<thead>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Сategory</th>
					<th>PriceOrigin</th> 
					<th>PriceDiscount</th>
					<th>Discount</th>
					<th>ProductOfDay</th>
					
				</thead>
				{{range . }}
				<tr>
					<td>{{.Model.ID}}</td>
					<td>{{.Name}}</td>
					<td>{{.Description}}</td>
					<td>{{.Сategory}}</td>
					<td>{{.PriceOrigin}}</td>
					<td>{{.PriceDiscount}}</td>
					<td>{{.Discount}}</td>
					<td>{{.ProductOfDay}}</td>
				</tr>
			{{end}}
			</table>`

	var w bytes.Buffer
	tmpl, err := template.New("items").Parse(html)
	if err != nil {

		return nil, err
	}

	err = tmpl.Execute(&w, r.obj)
	if err != nil {

		return nil, err
	}

	return w.Bytes(), nil
}