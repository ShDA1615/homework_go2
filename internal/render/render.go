package render

type Renderer interface {
	Render(params ...interface{}) (interface{}, error)
}