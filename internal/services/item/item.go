package item

import "gorm.io/gorm"

type Service struct {
	DB *gorm.DB
}

type Item struct {
	gorm.Model
	Name        string  `json:"Name"`
	Description string  `json:"Description"`
	Сategory string `json:"Сategory"`
	PriceOrigin       float32 `json:"Price"`
	PriceDiscount float32 `json:"PriceDiscount"`
	Discount float32 `json:"Discount"`
	ProductOfDay bool `json:"ProductOfDay"`


}

type ItemService interface {
	GetAllItemss() ([]Item, error)
	GetItemByID(ID uint) (Item, error)
	PostItem(item Item) (Item, error)
	UpdateItem(ID uint, newItem Item) (Item, error)
	DeleteItem(ID uint) error
}

func NewService(db *gorm.DB) *Service {
	return &Service{
		DB: db,
	}
}

func (s *Service) GetItems (ID uint) ([]Item, error) {
	//var items []Item
	if ID == 0 {
		items, err :=s.GetAllItems()
		return items, err
	} else {
		var items []Item
		item, err := s.GetItemByID(ID)
		items = append(items, item)
		return items, err
	}
}

func (s *Service) GetAllItems() ([]Item, error) {
	var items []Item
	if result := s.DB.Find(&items); result.Error != nil {
		return items, result.Error
	}

	return items, nil
}

func (s *Service) GetItemByID(ID uint) (Item, error) {
	var item Item
	result := s.DB.First(&item, ID)
	if result.Error != nil {
		return Item{}, result.Error
	}
	return item, nil
}


func (s *Service) PostItem(item Item) (Item, error) {
	if result := s.DB.Save(&item); result.Error != nil {
		return Item{}, result.Error
	}

	return item, nil
}

func (s *Service) UpdateItem(ID uint, newItem Item) (Item, error) {
	item, err := s.GetItemByID(ID)
	if err != nil {
		return Item{}, err
	}

	if result := s.DB.Model(&item).Updates(newItem); result.Error != nil {
		return Item{}, result.Error
	}
	return item, nil
}



func (s *Service) DeleteItem(ID uint) error {
	if result := s.DB.Delete(&Item{}, ID); result.Error != nil {
		return result.Error
	}

	return nil
}
