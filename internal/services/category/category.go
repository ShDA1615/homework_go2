package category
//Решение от https://github.com/Kolesov-Dmitry/homework

type Category struct {
	ID       int64
	Slug     string 
	Name     string 
	ParentID int64 

	
	parent   *Category
	children []*Category
}


func NewCategory(slug string, name string,id int64) *Category {
	return &Category{
		ID:       id,
		Slug:     slug,
		Name:     name,
		ParentID: 0,
		parent:   nil,
		children: make([]*Category, 0),
	}
}


func (c *Category) AppendSubcategory(category *Category) *Category {
	category.ParentID = c.ID
	category.parent = c

	c.children = append(c.children, category)

	return category
}


func (c *Category) Parent() *Category {
	return c.parent
}


func (c *Category) Root() *Category {
	if c.parent == nil {
		return c
	}

	return c.parent.Root()
}


func (c *Category) BreadCrumbs() []*Category {
	if c.parent == nil {
		return []*Category{c}
	}

	return append(c.parent.BreadCrumbs(), c)
}