package database

import (
	"github.com/ShDA1615/homework/internal/services/item"
	"gorm.io/gorm"
)

func MigrateDB(db *gorm.DB) error {
	if err := db.AutoMigrate(&item.Item{}); err != nil {
		return err
	}
	return nil
}
