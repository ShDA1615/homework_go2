package http

import (
	"encoding/json"
	"log"

	//"fmt"
	"github.com/ShDA1615/homework/internal/jobs"
	"github.com/ShDA1615/homework/internal/services/item"
	//"html/template"
	"net/http"
	"strconv"

	"github.com/ShDA1615/homework/internal/render"

	"github.com/gorilla/mux"
)

func NewHandler(service *item.Service, job *jobs.JobService) *Handler {
	return &Handler{
		Service: service,
		Job: job,
	}
}

type Handler struct {
	Router  *mux.Router
	Service *item.Service
	Job *jobs.JobService
}

func (h *Handler) InitRoutes() {
	h.Router = mux.NewRouter()
	h.Router.HandleFunc("/items", h.GetItems).Methods("GET")
	h.Router.HandleFunc("/items/{id}", h.GetItems).Methods("GET")
	h.Router.HandleFunc("/api/items", h.GetAllItems).Methods("GET")
	h.Router.HandleFunc("/api/items", h.PostItem).Methods("POST")
	h.Router.HandleFunc("/api/items/{id}", h.GetItemByID).Methods("GET")
	h.Router.HandleFunc("/api/items/{id}", h.UpdateItem).Methods("PUT")
	h.Router.HandleFunc("/api/items/{id}", h.DeleteItem).Methods("DELETE")
	h.Router.HandleFunc("/api/status", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(Response{Message: "Status OK!"}); err != nil {
			panic(err)
		}
	})
}

func (h *Handler) GetItems(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		id = "0"
	}
	itemID, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		respondWithError(w, "Ошибка преобразования ", err)
	}

	items, err := h.Service.GetItems(uint(itemID))
	if err != nil {
		respondWithError(w, "Ошибка получения товара", err)
	}

	content, err := render.NewHTMLRenderer(items).Render()
	if err != nil {
		log.Println(err)
	}
	w.Write(content)

	// html := `<table>
	// 			<thead>
	// 				<th>ID</th>
	// 				<th>Name</th>
	// 				<th>Description</th>
	// 				<th>Сategory</th>
	// 				<th>PriceOrigin</th>
	// 				<th>PriceDiscount</th>
	// 				<th>Discount</th>
	// 				<th>ProductOfDay</th>
	// 			</thead>
	// 			{{range . }}
	// 			<tr>
	// 				<td>{{.Model.ID}}</td>
	// 				<td>{{.Name}}</td>
	// 				<td>{{.Description}}</td>
	// 				<td>{{.Сategory}}</td>
	// 				<td>{{.PriceOrigin}}</td>
	// 				<td>{{.PriceDiscount}}</td>
	// 				<td>{{.Discount}}</td>
	// 				<td>{{.ProductOfDay}}</td>
	// 			</tr>
	// 		{{end}}
	// 		</table>`

	// tmpl, err := template.New("items").Parse(html)
	// if err != nil {
	// 	http.Error(w, err.Error(), 400)
	// 	return
	// }

	// err = tmpl.ExecuteTemplate(w, "items", items)
	// if err != nil {
	// 	http.Error(w, err.Error(), 400)
	// 	return
	// }

	//w.Write(content)

}

func (h *Handler) GetAllItems(w http.ResponseWriter, r *http.Request) {

	format := r.URL.Query().Get("format")
	if format == "html" {
		http.Redirect(w, r, "/items", http.StatusPermanentRedirect)
	} else {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		items, err := h.Service.GetAllItems()
		if err != nil {
			respondWithError(w, "Ошибка при выборе всех товаров", err)
		}
		// if err := json.NewEncoder(w).Encode(items); err != nil {
		// 	panic(err)
		// }

		content, err := render.NewJsonRenderer(items).Render()
		if err != nil {
			log.Println(err)
		}
		w.Write(content)
	}

}

func (h *Handler) PostItem(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	var item item.Item
	if err := json.NewDecoder(r.Body).Decode(&item); err != nil {
		respondWithError(w, "Не удалось преобразование JSON", err)
	}

	item, err := h.Service.PostItem(item)
	if err != nil {
		respondWithError(w, "Ошибка при создании товара", err)
	}

	if err = json.NewEncoder(w).Encode(item); err != nil {
		panic(err)
	}
}

func (h *Handler) GetItemByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	vars := mux.Vars(r)
	id := vars["id"]
	itemID, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		respondWithError(w, "Ошибка преобразования ", err)
	}
	item, err := h.Service.GetItemByID(uint(itemID))
	if err != nil {
		respondWithError(w, "Ошибка получения товара по ID", err)
	}
	if err := json.NewEncoder(w).Encode(item); err != nil {
		panic(err)
	}
}
func (h *Handler) UpdateItem(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	vars := mux.Vars(r)
	id := vars["id"]
	var item item.Item
	if err := json.NewDecoder(r.Body).Decode(&item); err != nil {
		respondWithError(w, "Ошибка преодразования JSON", err)
	}
	itemID, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		respondWithError(w, "Ошибка преобразования", err)
	}
	item, err = h.Service.UpdateItem(uint(itemID), item)
	if err != nil {
		respondWithError(w, "Ошибка при обновлении товара", err)
	}
	if err = json.NewEncoder(w).Encode(item); err != nil {
		panic(err)
	}
}
func (h *Handler) DeleteItem(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	vars := mux.Vars(r)
	id := vars["id"]

	itemID, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		respondWithError(w, "Ошибка преобразования", err)
	}

	err = h.Service.DeleteItem(uint(itemID))
	if err != nil {
		respondWithError(w, "Ошибка удаления товара по ID", err)
	}

	if err := json.NewEncoder(w).Encode(Response{Message: "Товар успешно удален"}); err != nil {
		panic(err)
	}
}
