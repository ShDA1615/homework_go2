package jobs

import (
	"time"
	"github.com/go-co-op/gocron"
)

type Task struct {
	Name string
	Schedule string 
	Job func()
}

type JobService struct {
	Cron *gocron.Scheduler
//	Tasks []Task
}

 
func InitCron(timeZone string) (*gocron.Scheduler, error) {
	 
	 location, err := time.LoadLocation(timeZone)
	 if err != nil {
		 return nil, err
	 }
	 return gocron.NewScheduler(location),nil

}
func NewService(c *gocron.Scheduler) *JobService {
	return &JobService{
		Cron: c,
		//Tasks:make([]Task, 0),
	}
}

func (jb *JobService) GetTaskCount() int {
	return jb.Cron.Len()
}

func (jb *JobService) Start() {
	// TODO: StartAsync(), StartBlocking()
	
}
func (jb *JobService) Stop()  {
	// TODO:
	 jb.Cron.Stop()
}

func NewTask() *Task {
	t:= &Task{
// TODO:
	}
	return t
}

func (jb *JobService) NewTask(t *Task) error{
       // TODO:
	   return nil
}

