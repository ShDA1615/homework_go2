package test

import (
	"testing"

	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
)

const BASE_URL = "http://localhost:9000"

func TestGetComments(t *testing.T) {
	client := resty.New()
	resp, err := client.R().Get(BASE_URL + "/api/items")
	if err != nil {
		t.Fail()
	}

	assert.Equal(t, 200, resp.StatusCode())
}

func TestPostItem(t *testing.T) {
	client := resty.New()
	resp, err := client.R().
		SetBody(`{"name": "Тест1", "description": "Описание1", "price": 28}`).
		Post(BASE_URL + "/api/items")

	assert.NoError(t, err)

	assert.Equal(t, 200, resp.StatusCode())
}
