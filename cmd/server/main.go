package main

import (
	"fmt"
	"net/http"

	"github.com/ShDA1615/homework/internal/database"
	internalHttp "github.com/ShDA1615/homework/internal/http"
	"github.com/ShDA1615/homework/internal/jobs"
	"github.com/ShDA1615/homework/internal/services/item"
)

func Run() error {
	fmt.Println("Сервер запущен")

	db, err := database.InitDatabase()
	if err != nil {
		return err
	}

	err = database.MigrateDB(db)
	if err != nil {
		return err
	}

//TODO: сделать изменяемой времянную зону
	cr, err:=jobs.InitCron("Europe/Moscow")
	if err != nil {
		return err
	}

	handler := internalHttp.NewHandler(item.NewService(db),jobs.NewService(cr))
	handler.InitRoutes()
	if err := http.ListenAndServe(":9000", handler.Router); err != nil {
		return err
	}
	return nil
}

func main() {
	err := Run()
	if err != nil {
		fmt.Println("Ошибка")
		fmt.Println(err)
	}
}
