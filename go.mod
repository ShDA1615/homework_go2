module github.com/ShDA1615/homework

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-co-op/gocron v1.5.0 // indirect
	github.com/go-resty/resty/v2 v2.6.0
	github.com/gorilla/mux v1.8.0
	//github.com/kr/pretty v0.2.1 // indirect
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20210423184538-5f58ad60dda6 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.8
)
